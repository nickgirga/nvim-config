# nvim-config
This is just a simple [Neovim](https://neovim.io/) config I created for myself, optimized for use with both [Python](https://www.python.org/) and [GDScript](https://docs.godotengine.org/en/stable/)/[Godot](https://godotengine.org/). It utilizes [vim-plug](https://github.com/junegunn/vim-plug) and some plugins may have other external dependencies. I know one of them requires [nodejs](https://nodejs.org/en/download/) and another requires [neovim-remote](https://github.com/mhinz/neovim-remote). Tracking down dependencies should be rather simple considering this is a pretty lightweight config with few plugins and few that require manual dependency installation. You may not need to install anything more than the 2 dependencies stated earlier.

### Installation
###### Install Script
You can simply clone the repository and run the installer script to setup the 'nickgirga/nvim-config' configuration in one line (granted you have the needed dependencies installed already):
`git clone https://gitlab.com/nickgirga/nvim-config.git && cd ./nvim-config && bash ./install.sh`.

Now, open Neovim and run `:PlugInstall` to install the new plugins. Once complete, just quit and restart Neovim. It should be ready to use now (you may want to run `:PlugStatus` to check the status of the new plugins).

###### Manual Installation
The config sits at `$PROJECT_ROOT/.config/nvim/init.vim` and should be placed at `$HOME/.config/nvim/init.vim` if using Neovim.
There is an additional language server config (for [coc](https://github.com/neoclide/coc.nvim)) that sits at `$PROJECT_ROOT/.config/nvim/coc-settings.json` and should be placed at `$HOME/.config/nvim/coc-settings.json` to utilize the GDScript language server (it's extremely helpful if you work with GDScript).

Now, open Neovim and run `:PlugInstall` to install the new plugins. Once complete, just quit and restart Neovim. It should be ready to use now (you may want to run `:PlugStatus` to check the status of the new plugins).

### Godot Configuration
You should configure Godot to talk to Neovim by opening your project, going to `Editor` (file menu) > `Editor Settings...` (file menu button) > `General` (tab) > `Text Editor` (category group in sidebar) > `External` (category) and setting the following values:

| Property | Value |
| ------ | ------ |
| Use External Editor | ✅ On |
| Exec Path | `$HOME/.local/bin/nvr` if neovim-remote was installed using [pip](https://pypi.org/project/pip/) (you may need to change `$HOME` to the actual path to your home directory) |
| Exec Flags | ```--servername godothost --remote-send "<C-\><C-N>:n {file}<CR>:call cursor({line},{col})<CR>"``` |

Once configured, you can start Neovim with `nvim --listen godothost .` in the root of your Godot project directory (it should have the `project.godot` file) to start listening for Godot. Then, open your project in Godot, double click a file in the `FileSystem` tab, and head back to Neovim (it will not request window focus). It should have opened the file you double clicked while in Godot.

For convenience, I recommend creating an alias in your user's `$HOME/.bashrc` (if you use bash) with the line `alias ':gdserver'='nvim --listen godothost +"VimFilerExplorer -project ."'`. Restart your terminal to apply the changes to `.bashrc`. Now, you can simply navigate to your desired project directory and type `:gdserver` (in bash, not Neovim). This will open Neovim with [vimfiler](https://github.com/Shougo/vimfiler.vim) in explorer mode with the current working directory as your project directory, already listening to Godot. You may need to switch windows to the text editor before opening a file using Godot (using Ctrl+W to enter window manipulation mode, followed by an arrow key to switch windows in the direction of the arrow key if you have default Neovim keybindings). Otherwise, when you go to open a file, it will replace the vimfiler explorer in the panel on the left instead of replacing the empty text document.
