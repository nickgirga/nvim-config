#!/usr/bin/bash
echo
echo \'nickgirga/nvim-config\' Installer
echo Copyright \(c\) 2022 Nicholas Girga
echo
echo WARNING!: This script will replace Neovim and Coc configuration files. 
echo The original files will be backed up as hidden files in their original configuration directory. 
echo You may want to copy some settings from your old configuration into the new one. 
echo
read -r -p "Are you sure you wish to continue? [y/N] > " response
lowercase_response=`echo $response | tr '[:upper:]' '[:lower:]'`
if [[ $lowercase_response != "y" ]];
then
	echo
	echo Quitting \(user request\)... # quit unless user responds with y/Y
	echo
	exit
fi

# Start of backing up user configuration
echo
echo Backing up original configuration files...

file_date_and_time=`date +"%Y-%m-%d_%H-%M-%S-%N"` # get the current date and time

config_dir="$HOME/.config/nvim"	# where user configuration files for Neovim and Coc are stored
backup_ext="nvim-config.${file_date_and_time}.backup" # the extension to append to backed up files

mkdir -p "$config_dir" # create directories if they do not exist

init_orig="$config_dir/init.vim"
if [ -f "$init_orig" ];
then
	init_backup="$config_dir/.init.vim.$backup_ext"
	cp "$init_orig" "$init_backup" # back up the user's init.vim
	echo \"$init_orig\" has been backed up to \"$init_backup\"!
else
	echo No configuration file found at path \($init_orig\). No backup needed.
fi

coc_orig="$config_dir/coc-settings.json"
if [ -f "$coc_orig" ];
then
	coc_backup="$config_dir/.coc_settings.json.$backup_ext"
	cp "$coc_orig" "$coc_backup" # back up the user's coc-settings.json
	echo \"$coc_orig\" has been backed up to \"$coc_backup\"!
else
	echo No configuration file found at path \($coc_orig\). No backup needed.
fi

echo Finished backing up original configuration files!
# End of backing up user configuration

# Install vim-plug according to official installation instructions for Unix/Linux (https://github.com/junegunn/vim-plug#unix-linux)
if ! [ -f "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim ];
then
	echo
	echo Installing vim-plug to Neovim...
	sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
    		https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	echo Finished installing vim-plug to Neovim!
fi

# Start of installation of 'nickgirga/nvim-config' configuration
echo
echo Installing \'nickgirga/nvim-config\' configuration files...

new_config_dir="`pwd`/.config/nvim" # where the 'nickgirga/nvim-config' configuration files are stored

init_new="$new_config_dir/init.vim"
cp "$init_new" "$init_orig" # install the 'nickgirga/nvim-config' Neovim configuration file
echo \"$init_new\" has been copied to \"$init_orig\"!

coc_new="$new_config_dir/coc-settings.json"
cp "$coc_new" "$coc_orig" # install the 'nickgirga/nvim-config' Coc configuration file
echo \"$coc_new\" has been copied to \"$coc_orig\"!

echo Finished installing \'nickgirga/nvim-config\' configuration files!
echo
echo Done!
echo
echo Remeber to run \`:PlugInstall\` when you first start Neovim to install all of the new plugins!
echo
# End of installation of 'nickgirga/nvim-config' configuration
