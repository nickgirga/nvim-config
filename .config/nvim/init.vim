" set text settings
colorscheme desert	" set color theme
set foldmethod=indent	" enable code folding
set number		" enable line numbers
set nowrap		" disable text wrapping

" setup plugins
call plug#begin('~/.config/nvim/plugins')					" define plugin directory

Plug 'habamax/vim-godot'							" define Godot plugin

Plug 'neoclide/coc.nvim', {'branch': 'release'}					" define coc.nvim plugin (aims to be VSCode-like)

Plug 'Shougo/unite.vim'								" define unite plugin (dependency of vimfiler plugin)

Plug 'Shougo/vimfiler.vim'							" define vimfiler plugin
let g:vimfiler_as_default_explorer = 1						" set vimfiler to default file explorer in Neovim
let g:vimfiler_tree_leaf_icon = ""						" clean up tree 'branches'
let g:vimfiler_tree_opened_icon = " 📂"						" use pretty opened directory icon
let g:vimfiler_tree_closed_icon = " 📁"						" use pretty closed directory icon
let g:vimfiler_file_icon = " 📄"						" use pretty file icon
let g:vimfiler_tree_indentation = 2						" make parent/child relationship more obvious (1 may be preferable on small displays)

Plug 'tpope/vim-fugitive'							" define Git plugin

Plug 'numirias/semshi', { 'do': ':UpdateRemotePlugins' }			" define semantic highlighting plugin for Python

call plug#end()									" initialize plugin system

" fold using space
nnoremap <space> za
